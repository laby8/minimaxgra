
public class Main {

    public static void main(String[] args) {
       Map map = new Map();
        map.show();
        ValAndPos result;
        while(map.terminal() == ""){
            result = miniMax(map,9,-10000,10000);
            map.move(result.pos);
            map.wpos = result.pos;
            map.show();
        }
    }



    public static ValAndPos miniMax(Map map,int d,int alpha,int beta){
        String result = map.terminal();
        ValAndPos valAndPos = new ValAndPos();
        if(result !="")
            return valAndPos;
        if(d == 0){
            valAndPos.val = map.score();
            return (valAndPos);
        }
        if(map.wolfTurn == true)
        for(Position action : map.actions()) {
            map.move(action);
            valAndPos = miniMax(map,--d,alpha, beta);
            map.moveBack(action);

            if(valAndPos.val > alpha) {
                alpha = valAndPos.val;
                valAndPos.pos = action;
            }
            else
                valAndPos.val = alpha;
            if(valAndPos.val >= beta)
                break;
        }
        else {
//            valAndPos.pos = null;
//            for(Position action : map.actions()) {
//                map.move(action);
//                valAndPos = miniMax(map,--d,alpha, beta);
//                map.moveBack(action);
            map.wolfTurn = true;
            valAndPos.val = beta;
        }
            return valAndPos;
    }
}