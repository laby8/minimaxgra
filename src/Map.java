import java.util.ArrayList;

public class Map {
    char[][] map;
    Position wpos;
    boolean wolfTurn;
   Map(){
       wolfTurn = true;
       wpos = new Position(2,0);
        map = new char[4][4];
        for(int i = 0;i<4;i++)
            for(int j = 0; j < 4;j++)
                map[i][j] = '-';
        map[3][3] = 'o';
        map[3][2] = 'o';
        map[3][0] = 'o';
        map[wpos.y][wpos.x] = 'w';

    }

    public void show()
    {
        for(int i = 0;i<4;i++) {
            for (int j = 0; j < 4; j++)
                System.out.print(map[i][j]);
            System.out.println();
        }
    }

    public ArrayList<Position> actions() {
        ArrayList<Position> actions;
        actions = new ArrayList<>();
        int i = wpos.y;
        int j = wpos.x;
        while(i > 0 && j < 3) {
            i--;
            j++;
            actions.add(new Position(j,i));
        }
        i = wpos.y;
        j = wpos.x;

        while(i < 3 && j < 3) {
            i++;
            j++;
            actions.add(new Position(j,i));
        }
        i = wpos.y;
        j = wpos.x;

        while(i < 3 && j > 0) {
            i++;
            j--;
            actions.add(new Position(j,i));
        }
        i = wpos.y;
        j = wpos.x;
        while(i > 0 && j > 0) {
            i--;
            j--;
            actions.add(new Position(j,i));
        }
        return actions;

    }
    public int score() {
       int result = 0;
       if(wpos.y == 3)
           result=result+10;
        if(wpos.y == 0)
            result=result-10;
        return result;
    }

    public void move(Position nextPos){
       if(wolfTurn == true) {
           map[nextPos.y][nextPos.x] = 'w';
           map[wpos.y][wpos.x] = '-';
           wpos = nextPos;
            wolfTurn = false;
       }
       else
           wolfTurn = true;
    }
    public void moveBack(Position nextPos){
       if(wolfTurn == true) {
           map[nextPos.y][nextPos.x] = '-';
           map[wpos.y][wpos.x] = 'w';
           wolfTurn = false;
       }
       else
           wolfTurn = true;
   }
    public boolean lookForSheeps(char[][] map){
        for(int i = 0;i<4;i++)
            for(int j = 0; j < 4;j++)
                if(map[i][j] == 'o')
                    return true;
                return false;
}


    public String terminal(){
        String result = "";
        if(!lookForSheeps(map))
            result="Wolf won!";
        return result;
    }
}
